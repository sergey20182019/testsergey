import './vendor';
import helpers from './helpers';
import './components/social';
import {ieFix} from './vendor/ie-fix';
import {vhFix} from './vendor/vh-fix';
import {actualYear} from './modules/actualYear';
import header from './components/header';
import lazyLoading from './modules/lazyLoading';
import scrollToAnchor from './modules/scrollToAnchor';

// import backToTop from './modules/backToTop';

import gsap from 'gsap';
import {ScrollTrigger} from 'gsap/ScrollTrigger';
gsap.registerPlugin(ScrollTrigger);
import './vendor/sticky-kit.min.js';

ieFix();
vhFix();
actualYear();
scrollToAnchor.init();
header.init();
lazyLoading.init();

// Sticky
$('#sticky').stick_in_parent({
	offset_top: 10,
});

// Header Fixed
function fixedHeader() {
	const fixedClass = 'is-fixed';

	helpers.$window.on('scroll', () => {
		const scrollTop = window.pageYOffset;
		const $header = $('.header');
		const headerOffset = $header.offset().top;

		if (scrollTop > headerOffset) {
			$header.addClass(fixedClass);
		} else {
			$header.removeClass(fixedClass);
		}
	});
}

fixedHeader();

// Back To Top
function backToTop() {
	const shownClass = 'is-shown';
	const className = '.js-back-to-top';

	helpers.$document.on('click.backTop', `${className}`, () => {
		helpers.scrollTo($('body'));
	});

	helpers.$window.on('scroll', () => {
		const scrollTop = window.pageYOffset;
		const $footer = $('.footer');
		const footerOffset = $footer.offset().top;

		if (scrollTop + window.innerHeight > footerOffset) {
			$(className).addClass(shownClass);
		} else {
			$(className).removeClass(shownClass);
		}
	});
}

backToTop();

// Share
function share(type) {
	const windowWidth = 700;
	const windowHeight = 450;
	const positionTop = (screen.height - windowHeight) / 2;
	const positionLeft = (screen.width - windowWidth) / 2;
	const data = {
		url: location.href,
		title: $('meta[property="og:title"]').attr('content') || document.title,
		image: $('meta[property="og:image"]').attr('content') || '',
		text: $('meta[property="og:description"]').attr('content') || '',
	};
	let request = '';

	switch (type) {
		case 'fb':
			request = `https://www.facebook.com/sharer.php?u=${data.url}&t=${data.title}`;
			break;

		case 'vk':
			request = `https://vk.com/share.php?url=${data.url}&title=${data.title}&description=${data.text}&image=${data.image}`;
			break;

		case 'tg':
			request = `https://t.me/share/url?url=${data.url}&text=${data.title}`;
			break;

		default:
			break;
	}

	window.open(request || '', '', `toolbar=0,status=0,scrollbars=1,width=${windowWidth},height=${windowHeight},top=${positionTop},left=${positionLeft}`);
}

$('.social__link').on('click', (e) => {
	e.preventDefault();
	let type = $(this).attr('data-type');

	share(type);
});

// Progress Scroll
function progressScroll() {
	const $progress = $('.js-progress');
	const $progressCircle = $progress.find('circle:last-child');
	const $percent = $('.js-progress-percent');
	let valPercent = 0;

	function loadProgress() {
		const scrollTop = window.pageYOffset;
		const documentHeight = document.body.clientHeight;

		valPercent = Math.ceil(scrollTop / (documentHeight - window.innerHeight) * 100);

		if (valPercent === 100) {
			$progress.addClass('is-finish');
		} else {
			$progress.removeClass('is-finish');
		}

		$percent.html(`${valPercent} %`);

		valPercent = valPercent * 48 * 3 / 100;

		$progressCircle.attr('style', `stroke-dasharray: ${valPercent}px 450px;`);
	}

	loadProgress();

	helpers.$window.on('scroll', loadProgress);
}

progressScroll();

// Animation
$('[data-anim]').each((i, elem) => {
	gsap.to(elem, {
		onComplete() {
			$(elem).addClass('is-complete');
		},
	});
});

$('[data-anim="fadeInLeft"]').each((i, elem) => {
	gsap.to(elem, {
		scrollTrigger: elem,
		duration: 1,
		x: 0,
		opacity: 1,
	});
});

$('[data-anim="fadeInUp"]').each((i, elem) => {
	gsap.to(elem, {
		scrollTrigger: elem,
		duration: 1,
		y: 0,
		opacity: 1,
	});
});

$('[data-anim="flip"]').each((i, elem) => {
	gsap.to(elem, {
		scrollTrigger: elem,
		duration: 1,
		scaleX: 1,
		opacity: 1,
	});
});

$('[data-anim="heart"]').each((i, elem) => {
	gsap.to(elem, {
		scrollTrigger: elem,
		scale: 1.08,
		opacity: 1,
		duration: 0.8,
		repeat: -1,
		yoyo: true,
		ease: 'back',
	});
});

// Preloader
function preloader() {
	const $preloader = $('.preloader');
	const $preloaderImage = $preloader.find('.preloader__image');
	const preloaderWidth = $preloaderImage.innerWidth();
	const preloaderHeight = $preloaderImage.innerHeight();
	let moveX = window.innerWidth + preloaderWidth;
	let moveY = window.innerHeight + preloaderHeight;
	let images = document.images;
	let len = images.length;
	let counter = 0;

	function incrementCounter() {
		counter++;
		$preloaderImage.css('transform', `translate(${counter / len * moveX}px, ${counter / len * moveY * -1}px)`);

		if (counter === len) {
			setTimeout(() => {
				$preloader.addClass('is-loaded');
			}, 2000);
		}
	}

	[].forEach.call(images, (img) => {
		if (img.complete) {
			incrementCounter();
		} else {
			img.addEventListener('load', incrementCounter, false);
		}
	});
}

preloader();
