// import { boolean } from 'yargs';
import header from '../components/header';
import helpers from '../helpers';

/**
* Модуль "Плавный переход к якорю"
*/
const init = () => {
	const classAnchor = '.js-to-anchor';
	const classActive = 'is-active';
	const $anchors = $(classAnchor);

	helpers.$document.on('click.anchor', '.js-to-anchor', (e) => {
		e.preventDefault();
		e.stopPropagation();

		const id = $(e.currentTarget).attr('href');
		const speed = $(e.currentTarget).data('speed') || 500;
		const offset = -helpers.$header.find('.header__inner').outerHeight(true);
		if (helpers.isMobileMd()) {
			$('.js-burger').removeClass('is-active');
			header.closeMenu().then(() => {
				helpers.scrollTo($(id), speed, offset);
			});
		} else {
			helpers.scrollTo($(id), speed);
		}
	});

	helpers.$window.on('scroll.anchor', () => {
		const scrollTop = window.pageYOffset;

		$anchors.each((i, el) => {
			let href = $(el).attr('href');
			let offsetTop = $(href).offset().top;
			let offsetBottom = offsetTop + $(href).innerHeight();

			if (helpers.isMobileMd()) {
				offsetTop -= helpers.$header.find('.header__inner').outerHeight(true);
			}

			if (scrollTop + 10 >= offsetTop && scrollTop < offsetBottom) {
				$(`${classAnchor}.${classActive}`).removeClass(classActive);
				$(`${classAnchor}[href="${href}"]`).addClass(classActive);
			}
		});
	});
};

const destroy = () => {
	helpers.$document.off('.anchor');
};

export default {
	init,
	destroy,
};
