<?php

$protocol = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
$host = $protocol . '://' . $_SERVER['HTTP_HOST'];
$title = 'Сергей, HTML-верстальщик';
$description = 'Привет! Я занимаюсь адаптивной вёрсткой сайтов. Дополнительно осваиваю фрейморк VueJS. У себя выделил бы 3 основные качества: терпение, ответственность, пунктуальность. Веду дневник жизни и успеха, а также есть мечта - создать проект по осуществлению каких-то значимых и вдохновляющих мечт других людей.';
$image = $host . '/images/teenager.png';

// Uncomment the code below and fill in the pages if necessary
// $pages = [
// 	'/page/1' => [
// 		'title' => '',
// 		'description' => '',
// 		'image' => '/images/',
// 	],
// ];

$page = @$pages[$_SERVER['REQUEST_URI']];

if ($page) {
	$title = !is_null(@$page['title']) ? $page['title'] : $title;
	$description = !is_null(@$page['description']) ? $page['description'] : $description;
	$image = !is_null(@$page['image']) ? $host . $page['image'] : $image;
}

?>
